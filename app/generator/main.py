import pathlib
import subprocess
import re
from docxtpl import DocxTemplate
import smtplib
import os
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import psycopg2
import psycopg2.extras
from datetime import date
from app.generator.config import (
    LIBRE_OFFICE,
    TEMPLATE,
    OUT_FOLDER,
    TMP_FILE_PATH
)
import zipfile
from app.service.sqlalchemy import get_session_sync
from app.api.send_logs.models.SendLogs import SendLogsModel
from datetime import datetime
import logging


logging.basicConfig(level=logging.INFO, filename='/opt/app/consumer.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')


def send_log_set_status(send_log_id, status_name):
    db = next(get_session_sync())
    obj = db.query(SendLogsModel).get(send_log_id)
    obj.status = status_name
    db.commit()
    db.close()


def add_send_log(customer_id, date_, instruction_id, user_id):
    db = next(get_session_sync())
    db_send_logs = SendLogsModel(
        user_id=user_id,
        created_at=datetime.now(),
        customer_id=customer_id,
        date=date_,
        instruction_id=instruction_id,
        status='Поиск шаблонов'
    )
    db.add(db_send_logs)
    db.commit()
    session_id = db_send_logs.id
    db.close()
    return session_id


class LibreOfficeError(Exception):
    def __init__(self, output):
        self.output = output


def convert_to_pdf(input_docx, out_folder, timeout=None):
    args = [LIBRE_OFFICE, '--headless', '--convert-to', 'pdf', '--outdir', out_folder, input_docx]

    process = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=timeout)
    filename = re.search('-> (.*?) using filter', process.stdout.decode())
    print(process.stdout.decode())
    print(process.stderr.decode())

    if filename is None:
        raise LibreOfficeError(process.stdout.decode())
    else:
        return filename.group(1)


def generate(context: dict, template_file_name: str) -> pathlib.PurePath:
    # t1 = datetime.now().strftime("%d_%m_%y_%H_%M_%S_%f")
    # tmp_file = pathlib.PurePath(PATH, 'tmp', t1 + '.docx')
    filename = get_filename_from_path(template_file_name)
    new_file_path = pathlib.PurePath(TMP_FILE_PATH, filename)
    template_filepath = pathlib.PurePath(TEMPLATE, template_file_name)
    print(template_filepath)
    doc = DocxTemplate(template_filepath)
    doc.render(context)
    doc.save(str(new_file_path))

    return new_file_path

    # context = {
    #     'date': date,
    #     'ФИО_СДЛ_ИмП': fio,
    #     'tbl_contents': [
    #         {'dol': work, 'fio': fio1},
    #         {'dol': work, 'fio': fio1},
    #         {'dol': work, 'fio': fio1},
    #     ]
    # }
    # doc.render(context)
    # doc.save(tmp_file)
    # convert(tmp_file, out_file)
    # return out_file


def is_valid_email(email: str) -> bool:
    regex = re.compile(r"([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|\"([]!#-[^-~ \t]|(\\[\t -~]))+\")@([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|\[[\t -Z^-~]*])")
    if re.fullmatch(regex, str(email)):
        return True
    else:
        return False


def send_email(out_file, customer_contact_email: str = 'test'):
    conn = psycopg2.connect(database="finconsultcrm", user="finconsultcrm",
                            password="Windowsmustdie1~", host="78.36.197.114", port=55432)
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql = f'''
       select * from settings;
       '''
    cur.execute(sql)
    res = cur.fetchall()
    sen_email = 'ddimasss@gmail.com'
    gmail_login = 'ddimasss@gmail.com'
    gmail_pass = 'Whiteboard23'
    rec_email = 'ddimasss@mail.ru'
    for row in res:
        if row['setting_name'] == 'Почта отправки писем':
            sen_email = row['setting_value']
        if row['setting_name'] == 'Gmail login':
            gmail_login = row['setting_value']
        if row['setting_name'] == 'Gmail pass':
            gmail_pass = row['setting_value']
        if row['setting_name'] == 'Почтовые адреса получатели':
            rec_email = row['setting_value']
    if is_valid_email(customer_contact_email):
        rec_email += f', {customer_contact_email}'
    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.login(gmail_login, gmail_pass)
    msg = MIMEMultipart()
    message = 'Сообщение\nSend from Hostname'
    # Insert the text to the msg going by e-mail
    msg.attach(MIMEText(message, "plain"))
    msg['Subject'] = "Test subject"
    msg['From'] = sen_email
    msg['To'] = rec_email
    with open(out_file, "rb") as f:
        attach = MIMEApplication(f.read(), _subtype="zip")
    attach.add_header('Content-Disposition', 'attachment', filename=get_filename_from_path(out_file))
    msg.attach(attach)

    # Send the mail
    try:
        server.send_message(msg)
    except Exception as e:
        with open('/opt/app/email.log', 'a') as file:
            file.write(str(e))
    finally:
        server.quit()


def get_fio(l_name: str, f_name: str, s_name: str):
    if l_name is None:
        l_name = 'None'
    if f_name is None:
        f_name = 'None'
    if s_name is None:
        s_name = 'None'
    fio = l_name + ' ' + f_name[0] + '.' + s_name[0] + '.'
    return fio


def get_fio_full(l_name: str, f_name: str, s_name: str):
    if l_name is None:
        l_name = 'None'
    if f_name is None:
        f_name = 'None'
    if s_name is None:
        s_name = 'None'
    fio = l_name + ' ' + f_name + ' ' + s_name
    return fio


def get_from_db(
        date_for_generation: datetime,
        customer_id: int = None,
        types: list = None,
        gen_start_date: bool = True,
        date_end: datetime = datetime.now(),
):
    if types is None:
        types = []
    conn = psycopg2.connect(database="finconsultcrm", user="finconsultcrm",
                            password="Windowsmustdie1~", host="78.36.197.114", port=55432)
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql = f'''
    select
        t.id as template_id,
        t.doc_start_date as template_doc_start_date,
        t.employee_count as template_employee_count,
        t.file_path as template_file_path,
        t.gen_start_date as template_gen_start_date,
        t."name" as template_name,
        t.npa_date as template_npa_date,
        t.npa_name as template_npa_name,
        t.instruction_id as template_instruction_id,
        c.id as customer_id,
        c.contact_name as customer_contact_name,
        c.contact_phone as customer_contact_phone,
        c.contract_date_end as customer_contract_date,
        actual_adr.address as customer_fact_address,
        c.full_name as customer_full_name,
        c.itn as customer_itn,
        c.kpp as customer_kpp,
        c.ogrn as customer_ogrn,
        c.phone as customer_phone,
        c.contact_email as customer_contact_email,
        mailing_adr.address as customer_post_address,
        c.short_name as customer_short_name,
        legal_adr.address as customer_ur_address,
        ctcc.customer_category_id as customer_category_id,
        ttcc.customer_category_id as template_castomer_category_id,
        e3.employee_count as customer_employees_count,
        e2.employees as customer_employees,
        sdl1.f_name as sdl_f_name,
        sdl1.l_name as sdl_l_name,
        sdl1.s_name as sdl_s_name,
        sdl1.l_name_r as sdl_l_name_r,
        sdl1.l_name_d as sdl_l_name_d,
        sdl1.l_name_v as sdl_l_name_v,
        sdl1.l_name_t as sdl_l_name_t,
        sdl1."name_i" as sdl_position_name_i,
        sdl1."name_r" as sdl_position_name_r,
        sdl1."name_d" as sdl_position_name_d,
        sdl1."name_t" as sdl_position_name_t,
        sdl1."name_v" as sdl_position_name_v,
        ruk.f_name as ruk_f_name,
        ruk.l_name as ruk_l_name,
        ruk.s_name as ruk_s_name,
        ruk.l_name_r as ruk_l_name_r,
        ruk.l_name_d as ruk_l_name_d,
        ruk.l_name_v as ruk_l_name_v,
        ruk.l_name_t as ruk_l_name_t,
        ruk."name_i" as ruk_position_name_i,
        ruk."name_r" as ruk_position_name_r,
        ruk."name_d" as ruk_position_name_d,
        ruk."name_t" as ruk_position_name_t,
        ruk."name_v" as ruk_position_name_v,
        ct.full_name as company_type_full_name,
        ct.short_name as company_type_short_name,
        o."name"  as okved_name,
        o.okved as okved_number
    FROM templates t
    left join template_to_company_type ttct on (t.id = ttct.template_id)
    left join template_to_customer_category ttcc on (ttcc.template_id = t.id)
    left join customers c on (ttct.company_type_id = c.company_type_id and c.is_deleted = false)
    left join company_types ct on (ct.id = c.company_type_id)
    left join customer_to_customer_category ctcc on (c.id = ctcc.customer_id)
    left join customer_to_okveds cto on (c.id = cto.customer_id and cto.type = 'main')
    left join okveds o on (o.id = cto.okved_id)
    left join addresses legal_adr on (
            c.id = legal_adr.customer_id
            and legal_adr.type='LEGAL'
            and cast( '{date_for_generation.strftime("%Y-%m-%d")}' as date) >= legal_adr.date_start
            and (cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) <= legal_adr.date_end or legal_adr.date_end is NULL)
        )
    left join addresses actual_adr on (
            c.id = actual_adr.customer_id
            and actual_adr.type='ACTUAL'
            and cast( '{date_for_generation.strftime("%Y-%m-%d")}' as date) >= actual_adr.date_start
            and (cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) <= actual_adr.date_end or actual_adr.date_end is NULL)
        )
    left join addresses mailing_adr on (
            c.id = mailing_adr.customer_id
            and mailing_adr.type='MAILING'
            and cast( '{date_for_generation.strftime("%Y-%m-%d")}' as date) >= mailing_adr.date_start
            and (cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) <= mailing_adr.date_end or mailing_adr.date_end is NULL)
        )
    left join (
        select
            customer_id,
            count(*) as employee_count
        FROM public.employees e
        where
            cast( '{date_for_generation.strftime("%Y-%m-%d")}' as date) >= e.start_date
            and (cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) <= e.end_date or e.end_date is NULL)
        group by customer_id
    ) e3 on (e3.customer_id = c.id)
    left join (
                SELECT
                    e.customer_id,
                    JSON_AGG(
                        JSON_BUILD_OBJECT(
                            'emp_id', e.id,
                            'emp_f_name', f_name,
                            'emp_l_name', l_name,
                            'emp_s_name', s_name,
                            'emp_l_name_r', l_name_r,
                            'emp_l_name_d', l_name_d,
                            'emp_l_name_v', l_name_v,
                            'emp_l_name_t', l_name_t,
                            'emp_position_desc', p.description,
                            'emp_position_name_i', p.name_i,
                            'emp_position_name_r', p.name_r,
                            'emp_position_name_d', p.name_d,
                            'emp_position_name_v', p.name_v,
                            'emp_position_name_t', p.name_t
                            )
                    ) as employees
                FROM public.employees e
                left join employees_to_positions etp on (etp.employee_id = e.id)
                left join positions p on (etp.position_id = p.id)
                where cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) >= etp.start_date
                    and (cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) <= etp.end_date or etp.end_date is NULL)
                    and cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) >= e.start_date
                    and (cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) <= e.end_date or e.end_date is NULL)
                group by e.customer_id
    ) e2 on (e2.customer_id = c.id)
    left join (SELECT
                    e.*,
                    e.customer_id as cust_id,
                    p2.*
                FROM public.employees e
                left join employees_to_positions etp on (etp.employee_id = e.id)
                left join positions p2 on (p2.id = etp.position_id)
                where p2.seo is True
                    --and cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) >= ceo.start_date
                    --and (cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) <= ceo.end_date or ceo.end_date is NULL)
                    and cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) >= e.start_date
                    and (cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) <= e.end_date or e.end_date is NULL)
                ) ruk on (ruk.cust_id = c.id)
    left join (SELECT
                    e.*,
                    e.customer_id as cust_id,
                    p2.*
                FROM public.employees e
                left join employees_to_positions etp on (etp.employee_id = e.id)
                left join positions p2 on (p2.id = etp.position_id)
                --left join sdl on (sdl.employee_id = e.id)
                where cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) >= e.sdl_start_date
                    --and (cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) <= sdl.end_date or sdl.end_date is NULL)
                    and cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) >= e.start_date
                    and (cast('{date_for_generation.strftime("%Y-%m-%d")}' as date) <= e.end_date or e.end_date is NULL)
                ) sdl1 on (sdl1.cust_id = c.id)
    where
        t.is_deleted = false
        and ctcc.customer_category_id = ttcc.customer_category_id
        and (e3.employee_count = t.employee_count or (t.employee_count = 2 and e3.employee_count > 1))
        '''

    if customer_id is not None:
        sql += f''' and c.id = {customer_id} '''
    if len(types) > 0:
        sql += f''' and t.instruction_id in ({",".join(map(str, types))})'''
    if gen_start_date:
        sql += f""" and t.gen_start_date = cast('{date_for_generation.strftime("%Y-%m-%d")}' as date)"""
    cur.execute(sql)
    records = cur.fetchall()
    logging.info(list(records))
    return records


def employees_parse(context: dict, employee_id: int = -1):
    employees = context['Сотрудники']
    for employee in employees:
        employee['ФИО_сотрудника_ИмП'] = get_fio(employee['emp_l_name'], employee['emp_f_name'], employee['emp_s_name'])
        employee['ФИО_сотрудника_РП'] = get_fio(employee['emp_l_name_r'], employee['emp_f_name'],
                                                employee['emp_s_name'])
        employee['ФИО_сотрудника_ВП'] = get_fio(employee['emp_l_name_v'], employee['emp_f_name'],
                                                employee['emp_s_name'])
        employee['ФИО_сотрудника_ТП'] = get_fio(employee['emp_l_name_t'], employee['emp_f_name'],
                                                employee['emp_s_name'])
        employee['ФИО_сотрудника_ДП'] = get_fio(employee['emp_l_name_d'], employee['emp_f_name'],
                                                employee['emp_s_name'])
        employee['Должность_сотрудника_ИмП'] = employee['emp_position_name_i']
        employee['Должность_сотрудника_РП'] = employee['emp_position_name_r']
        employee['Должность_сотрудника_ВП'] = employee['emp_position_name_v']
        employee['Должность_сотрудника_TП'] = employee['emp_position_name_t']
        employee['Должность_сотрудника_ДП'] = employee['emp_position_name_d']
        if employee['emp_id'] == employee_id:
            context['ФИО_сотрудника_ИмП'] = get_fio(employee['emp_l_name'], employee['emp_f_name'],
                                                    employee['emp_s_name'])
            context['ФИО_сотрудника_РП'] = get_fio(employee['emp_l_name_r'], employee['emp_f_name'],
                                                   employee['emp_s_name'])
            context['ФИО_сотрудника_ВП'] = get_fio(employee['emp_l_name_v'], employee['emp_f_name'],
                                                   employee['emp_s_name'])
            context['ФИО_сотрудника_ТП'] = get_fio(employee['emp_l_name_t'], employee['emp_f_name'],
                                                   employee['emp_s_name'])
            context['ФИО_сотрудника_ДП'] = get_fio(employee['emp_l_name_d'], employee['emp_f_name'],
                                                   employee['emp_s_name'])
            context['Должность_сотрудника_ИмП'] = employee['emp_position_name_i']
            context['Должность_сотрудника_РП'] = employee['emp_position_name_r']
            context['Должность_сотрудника_ВП'] = employee['emp_position_name_v']
            context['Должность_сотрудника_TП'] = employee['emp_position_name_t']
            context['Должность_сотрудника_ДП'] = employee['emp_position_name_d']


def get_context(record: dict):
    context_ = {
        'Дата_подписания_договора_с_клиентом': record['customer_contract_date'].strftime("%d.%m.%Y"),
        'Дата_начала_подготовки_документов': record['template_gen_start_date'].strftime("%d.%m.%Y"),
        'Контактное_лицо_клиента': record['customer_contact_name'],
        'Телефон_клиента': record['customer_contact_phone'],
        'email_клиента': 'testemail@test.ru',
        'Краткая_форма_собственности_клиента': record['company_type_short_name'],
        'Полная_форма_собственности_клиента': record['company_type_full_name'],
        'ФИО_руководителя_ИмП': get_fio(record['ruk_l_name'], record['ruk_f_name'], record['ruk_s_name']),
        'ФИО_руководителя_РП': get_fio(record['ruk_l_name_r'], record['ruk_f_name'], record['ruk_s_name']),
        'ФИО_руководителя_ВП': get_fio(record['ruk_l_name_v'], record['ruk_f_name'], record['ruk_s_name']),
        'ФИО_руководителя_ДП': get_fio(record['ruk_l_name_d'], record['ruk_f_name'], record['ruk_s_name']),
        'ФИО_руководителя_ТП': get_fio(record['ruk_l_name_t'], record['ruk_f_name'], record['ruk_s_name']),
        'Должность_руководителя_ИмП': record['ruk_position_name_i'],
        'Должность_руководителя_РП': record['ruk_position_name_r'],
        'Должность_руководителя_ВП': record['ruk_position_name_v'],
        'Должность_руководителя_ДП': record['ruk_position_name_d'],
        'Должность_руководителя_ТП': record['ruk_position_name_t'],
        'ФИО_СДЛ_ИмП': get_fio(record['sdl_l_name'], record['sdl_f_name'], record['sdl_s_name']),
        'ФИО_СДЛ_РП': get_fio(record['sdl_l_name_r'], record['sdl_f_name'], record['sdl_s_name']),
        'ФИО_СДЛ_ВП': get_fio(record['sdl_l_name_v'], record['sdl_f_name'], record['sdl_s_name']),
        'ФИО_СДЛ_ДП': get_fio(record['sdl_l_name_d'], record['sdl_f_name'], record['sdl_s_name']),
        'ФИО_СДЛ_ТП': get_fio(record['sdl_l_name_t'], record['sdl_f_name'], record['sdl_s_name']),
        'Должность_СДЛ_ИмП': record['sdl_position_name_t'],
        'Должность_СДЛ_РП': record['sdl_position_name_t'],
        'Должность_СДЛ_ВП': record['sdl_position_name_t'],
        'Должность_СДЛ_ДП': record['sdl_position_name_t'],
        'Должность_СДЛ_ТП': record['sdl_position_name_t'],
        'Краткое_наименование_клиента': record['customer_short_name'],
        'Полное_наименование_клиента': record['customer_full_name'],
        'Юридический_адрес_клиента': record['customer_ur_address'],
        'Фактический_адрес_клиента': record['customer_fact_address'],
        'Почтовый_адрес_клиента': record['customer_post_address'],
        'ИНН_клиента': record['customer_itn'],
        'КПП_клиента': record['customer_kpp'],
        'ОГРН_клиента': record['customer_ogrn'],
        'ОКВЭД_клиента': record['okved_number'] + ' ' + record['okved_name'],
        'ОКВЭД_название': record['okved_name'],
        'ОКВЭД_код': record['okved_number'],
        'Дата_вступления_документа_в_силу': record['template_doc_start_date'].strftime("%d.%m.%Y"),
        'Дата_НПА_дд_мм_гггг': record['template_npa_date'].strftime("%d.%m.%Y"),
        'Дата_НПА_ггггммдд': record['template_npa_date'].strftime("%Y%m%d"),
        'Наименование_НПА': record['template_npa_name'],
        'Сотрудники': record['customer_employees'],
        'file': record['template_file_path']
    }
    return context_


def generate_for_employee_change(date_for_generate: datetime, customer_id: int, employee_id):
    template_records = get_from_db(date_for_generation=date_for_generate, customer_id=customer_id, types=[1],
                                   gen_start_date=False)
    for template_record in template_records:
        context = get_context(template_record)
        employees_parse(context=context, employee_id=employee_id)
        generate(context=context, template_file_name=context['file'])


def generate_for_new_customer(customer_id: int):
    os.system('rm -rf %s/*' % str(OUT_FOLDER))
    template_records = get_from_db(date_for_generation=datetime.now(), customer_id=customer_id,
                                   gen_start_date=False)
    zip_ = pathlib.PurePath(OUT_FOLDER, 'generated.zip')
    zip_obj = zipfile.ZipFile(zip_, 'w')
    for template_record in template_records:
        context = get_context(template_record)
        employees_parse(context=context)
        new_file_path = generate(context=context, template_file_name=context['file'])
        filename = convert_to_pdf(str(new_file_path), str(OUT_FOLDER))
        zip_obj.write(filename, get_filename_from_path(filename))
    zip_obj.close()
    send_email(zip_)


def generate_for_customer_categories(customer_category_id: int, date: date, ids: list) -> int:
    pass


def generate_any(customer_id: int, gen_date: date, ids: list, user_id: int) -> int:
    with_file = True
    try:
        file = open('/opt/app/log1.txt', 'w')
    except Exception as e:
        print('log1 opening error: ', str(e))
        with_file = False

    if with_file:
        file.write('generating')
    log_ids = {}
    for i in ids:
        log_id = add_send_log(customer_id, gen_date, i, user_id)
        log_ids[i] = log_id

    os.system('rm -rf %s/*' % str(OUT_FOLDER))
    template_records = get_from_db(date_for_generation=gen_date, customer_id=customer_id,
                                   gen_start_date=False, types=ids)
    zip_ = pathlib.PurePath(OUT_FOLDER, 'generated.zip')
    zip_obj = zipfile.ZipFile(zip_, 'w')
    for_sending = False
    for template_record in template_records:
        inst_type = template_record['template_instruction_id']
        send_log_set_status(log_ids[inst_type], 'Начинаем заменять переменные')
        send_log_set_status(log_ids[inst_type], 'Ищем переменные в БД')
        try:
            context = get_context(template_record)
        except Exception as e:
            send_log_set_status(log_ids[inst_type], 'Ошибка получения шаблонов: ' + str(e))
        send_log_set_status(log_ids[inst_type], 'Ищем сотрудников в БД')
        try:
            employees_parse(context=context)
        except Exception as e:
            send_log_set_status(log_ids[inst_type], 'Ошибка парсера сотрудников: ' + str(e))
        send_log_set_status(log_ids[inst_type], 'Подставляем переменные в шаблон')
        try:
            new_file_path = generate(context=context, template_file_name=context['file'])
        except Exception as e:
            send_log_set_status(log_ids[inst_type], 'Ошибка генерации документа из шаблона: ' + str(e))
        send_log_set_status(log_ids[inst_type], 'Преобразуем в PDF')
        try:
            filename = convert_to_pdf(str(new_file_path), str(OUT_FOLDER))
        except Exception as e:
            send_log_set_status(log_ids[inst_type], 'Ошибка преобразования в PDF: ' + str(e))
        send_log_set_status(log_ids[inst_type], 'Собираем ZIP архив')
        try:
            zip_obj.write(filename, get_filename_from_path(filename))
        except Exception as e:
            send_log_set_status(log_ids[inst_type], 'Ошибка сохранения ZIP: ' + str(e))
        else:
            for_sending = True
    zip_obj.close()
    if for_sending:
        for template_record in template_records:
            inst_type = template_record['template_instruction_id']
            send_log_set_status(log_ids[inst_type], 'Отправляем письма')
        try:
            send_email(zip_, str(template_record['customer_contact_email']))
        except smtplib.SMTPException as e:
            send_log_set_status(log_ids[inst_type], 'Ошибка отправки писем: ' + str(e))
            return e
        except Exception as e:
            send_log_set_status(log_ids[inst_type], 'Ошибка отправки писем: ' + str(e))
            return e
        else:
            for template_record in template_records:
                inst_type = template_record['template_instruction_id']
                send_log_set_status(log_ids[inst_type], 'Готово')
            return len(template_records)
    if with_file:
        file.write('Конец ')
        file.close()


def generate_for_staff_update(customer_id: int):
    os.system('rm -rf %s/*' % str(OUT_FOLDER))
    template_records = get_from_db(date_for_generation=datetime.now(), customer_id=customer_id,
                                   gen_start_date=False, types=[1])
    zip_ = pathlib.PurePath(OUT_FOLDER, 'generated.zip')
    zip_obj = zipfile.ZipFile(zip_, 'w')
    for template_record in template_records:
        context = get_context(template_record)
        employees_parse(context=context)
        new_file_path = generate(context=context, template_file_name=context['file'])
        filename = convert_to_pdf(str(new_file_path), str(OUT_FOLDER))
        zip_obj.write(filename, get_filename_from_path(filename))
    zip_obj.close()
    send_email(zip_)


def get_filename_from_path(path: str) -> str:
    filename = os.path.split(path)[1]
    return filename


if __name__ == "__main__":
    # generate_for_employee_change(customer_id=1, employee_id=2)
    generate_for_new_customer(39)
    # dt = datetime.strptime('2021-10-11', "%Y-%m-%d")
    # records = get_from_db(date_for_generation=dt, customer_id=1, types=[1])
    # for record in records:
    #     print(record)
    #     context = get_context(record)
    #     # employees_parse(context['Сотрудники'])
    #     # print(context['file'])
    #     # generate(context=context, template_file_name=context['file'])
