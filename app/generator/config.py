import pathlib


PATH = pathlib.PurePath(pathlib.Path().resolve(), 'app', 'generator')
# PATH = pathlib.PurePath(pathlib.Path().resolve())
PATH_GEN = pathlib.PurePath(pathlib.Path().resolve())
OUT_FOLDER = pathlib.PurePath(PATH, '/tmp')
OUT_FILE = pathlib.PurePath(OUT_FOLDER, 'my_word_template.pdf')
TEMPLATE = pathlib.PurePath(PATH, 'templates')
TEMPLATE_GEN = pathlib.PurePath(PATH_GEN, 'templates')
TEMPLATE_FILE = pathlib.PurePath(PATH, 'templates', 'my_word_template.docx')
LIBRE_OFFICE = r'/usr/bin/soffice'
# LIBRE_OFFICE = r'/Applications/LibreOffice.app/Contents/MacOS/soffice'
TMP_FILE_PATH = pathlib.PurePath(PATH, 'tmp')
TMP_PATH = pathlib.PurePath(PATH, 'tmp')
TMP_FILE_GEN = pathlib.PurePath(PATH_GEN, 'tmp', 'my_word_template.docx')
OUT_FOLDER_GEN = pathlib.PurePath(PATH_GEN, 'generated')
OUT_FILE_GEN = pathlib.PurePath(OUT_FOLDER_GEN, 'my_word_template.pdf')
