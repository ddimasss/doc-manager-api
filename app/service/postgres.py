import psycopg2
from psycopg2.extras import NamedTupleCursor
from app.settings import settings


def _connect():
    return psycopg2.connect(
        **settings.POSTGRES_SETTINGS.dict(), 
        cursor_factory=NamedTupleCursor
    )


_conn = _connect()


def get_postgres_connection():
    global _conn
    try:
        with _conn:
            with _conn.cursor() as cur:
                cur.execute('SELECT 1')
    except psycopg2.InterfaceError:
        _conn.close()
        _conn = _connect()
    
    return _conn
