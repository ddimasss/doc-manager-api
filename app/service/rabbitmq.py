from aio_pika import Message, connect
from aio_pika.abc import AbstractIncomingMessage
import json
import datetime
import pika


async def producer(customer_id: int, gen_date: datetime.date, ids: list, user_id: int) -> None:
    # Perform connection
    connection = await connect("amqp://lim:Serial111@188.168.35.10/")
    mess = {
        'customer_id': customer_id,
        'date': gen_date.strftime("%Y-%m-%d"),
        'ids': ids,
        'user_id': user_id
    }
    mess = json.dumps(mess)
    async with connection:
        # Creating a channel
        channel = await connection.channel()

        # Declaring queue
        queue = await channel.declare_queue("hello")

        # Sending the message
        await channel.default_exchange.publish(
            Message(mess.encode()),
            routing_key=queue.name,
        )

        print(f" [x] Sent {mess}")


async def on_message(message: AbstractIncomingMessage) -> None:

    """
    on_message doesn't necessarily have to be defined as async.
    Here it is to show that it's possible.
    """
    a = json.loads(message.body.decode())
    d = datetime.datetime.strptime(a['date'], "%Y-%m-%d").date()
    print("Receive Message body is: %r" % message.body)
    #a = await generate_any(a['customer_id', d, a['ids']])
    print(a)


async def consumer() -> None:
    # Perform connection
    connection = await connect("amqp://lim:Serial111@188.168.35.10/")
    queue_name = "hello1"

    channel = await connection.channel()

    # Maximum message count which will be processing at the same time.
    await channel.set_qos(prefetch_count=10000)

    # Declaring queue
    queue = await channel.declare_queue(queue_name, auto_delete=True)
    await queue.consume(on_message)


def sync_consumer():
    credentials = pika.PlainCredentials('lim', 'Serial111')
    connection = pika.BlockingConnection(pika.ConnectionParameters('188.168.35.10',  credentials=credentials))
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    def callback(ch, method, properties, body):
        from app.generator.main import generate_any
        a = json.loads(body.decode())
        d = datetime.datetime.strptime(a['date'], "%Y-%m-%d").date()
        print('1------', a)
        i = generate_any(a['customer_id'], d, a['ids'],  a['user_id'])

        print(i)

    channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


if __name__ == '__main__':
    sync_consumer()
