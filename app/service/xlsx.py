from io import StringIO
from typing import List
from openpyxl import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from tempfile import NamedTemporaryFile

def make_xlsx(rows: List[List[str]] = [], sheet_name: str = "First") -> StringIO:
    book = Workbook()
    book.remove(book.active)
    book.create_sheet(title=sheet_name)
    sheet:Worksheet = book[sheet_name]

    for row in rows:
        sheet.append(row)

    with NamedTemporaryFile() as tmp:
        book.save(tmp.name)
        tmp.seek(0)
        stream = tmp.read()
    return stream
