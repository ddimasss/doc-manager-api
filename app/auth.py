import enum
import jwt
import base64
from typing import List, Any
from pydantic import BaseModel, Field
from fastapi import Depends, HTTPException
from fastapi.security import APIKeyHeader
from app.settings import settings


TOKEN_PREFIX = 'Bearer '
token_scheme = APIKeyHeader(name='Authorization')


class ForbiddenHTTPException(HTTPException):
    
    STATUS_CODE = 403
    
    def __init__(self, detail: Any = None) -> None:
        super().__init__(
            ForbiddenHTTPException.STATUS_CODE, 
            detail=detail
        )


class TokenPayload(BaseModel):

    class Details(BaseModel):
        user_id: int = Field(0, alias='userId')
        username: str = ''
        emails: List[str] = []
        phone_numbers: List[str] = Field([], alias='phoneNumbers')
        enabled: bool = False
        display_name: str = Field('', alias='displayName')
        bot: bool = False

    permissions: List[str] = []
    details = Details()
    msps: List[str] = []
    series_id: str = Field('', alias='seriesId')
    sub: str = ''
    iat: int = 0
    exp: int = 0


async def token_payload_dependency(
    token: str = Depends(token_scheme) if settings.ENABLE_AUTHORIZATION else None
) -> TokenPayload:
    if not settings.ENABLE_AUTHORIZATION:
        return TokenPayload()

    if not token.startswith(TOKEN_PREFIX):
        raise ForbiddenHTTPException()

    token = token[len(TOKEN_PREFIX):]
    try:
        jwt_secret = base64.b64decode(settings.JWT_SECRET_BASE64)
        payload = jwt.decode(token, jwt_secret, algorithms=['HS512'])
    except jwt.ExpiredSignatureError:
        raise ForbiddenHTTPException('Token had been expired')
    except jwt.InvalidSignatureError:
        raise ForbiddenHTTPException('Invalid token signature')
    except jwt.InvalidTokenError:
        raise ForbiddenHTTPException()

    return TokenPayload(**payload)


class PermissionsEnum(enum.Enum):
    ADMIN: str = 'admin'


def token_payload_with_permissions_dependency(
    permissions: List[PermissionsEnum]
):
    permissions.append(PermissionsEnum.ADMIN)

    async def dependency(
        token_payload: TokenPayload = Depends(token_payload_dependency)
    ):
        if not settings.ENABLE_AUTHORIZATION:
            return token_payload

        for permission in permissions:
            if permission.value in token_payload.permissions:
                return token_payload

        raise ForbiddenHTTPException('You do not have permissions to do this')

    return dependency
