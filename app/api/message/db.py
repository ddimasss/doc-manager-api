from sqlalchemy.ext.declarative import as_declarative


@as_declarative()
class Base:
    __tablename__ = 'messages'
    __table_args__ = {'schema': 'public'}
