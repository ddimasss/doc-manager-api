from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Message import MessageModel


async def get(
        message_id: int,
        db=Depends(get_session_sync)
):
    obj = db.query(MessageModel).get(message_id)
    db.commit()

    return obj


async def get_list(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    obj = db.query(MessageModel).limit(limit).offset(offset).all()
    res = obj

    return res
