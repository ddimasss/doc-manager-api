from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Message import MessageModel
from ..schemas.Message import MessageScheme


async def edit(
        message_request: MessageScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(MessageModel).get(message_request.id)
    obj.name = message_request.name
    obj.text = message_request.text
    obj.is_deleted = message_request.is_deleted
    db.commit()

    return obj
