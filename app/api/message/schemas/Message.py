from pydantic import BaseModel, Field
from enum import Enum
from datetime import date
from typing import List, Optional, Any


class MessageScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    name: str
    text: str
    is_deleted: Optional[bool] = False
