from fastapi import APIRouter
from .actions.add import add, create_upload_file
from .actions.edit import edit_upload_file
from .actions.delete import delete
from .actions.get import get, get_list
from .schemas.Template import TemplateScheme
from typing import List


template_router = APIRouter()


template_router.add_api_route(
    path='/add',
    endpoint=add,
    methods=['POST'],
    tags=['other'],
    response_model=TemplateScheme
)

template_router.add_api_route(
    path='/upload',
    endpoint=create_upload_file,
    methods=['POST'],
    tags=['other']
)

template_router.add_api_route(
    path='/edit_upload',
    endpoint=edit_upload_file,
    methods=['POST'],
    tags=['other']
)

template_router.add_api_route(
    path='/edit',
    endpoint=edit_upload_file,
    methods=['POST'],
    tags=['other'],
    response_model=TemplateScheme
)
template_router.add_api_route(
    path='/delete',
    endpoint=delete,
    methods=['POST'],
    tags=['other']
)

template_router.add_api_route(
    path='/get',
    endpoint=get,
    methods=['GET'],
    tags=['other'],
    response_model=TemplateScheme
)

template_router.add_api_route(
    path='/get_list',
    endpoint=get_list,
    methods=['GET'],
    tags=['other'],
    response_model=List[TemplateScheme]
)
