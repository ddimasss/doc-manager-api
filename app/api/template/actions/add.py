import re
from fastapi import Depends, Form, File, UploadFile
from app.service.sqlalchemy import get_session_sync
from ..models.Template import TemplateModel, TemplateToCompanyType, TemplateToCustomerCategory
from ..schemas.Template import TemplateScheme, TemplateAddScheme
from app.settings import settings
import os
import cProfile


async def add(
        template_request: TemplateScheme,
        db=Depends(get_session_sync)
):
    if 'id' in template_request:
        del template_request.id
    db_customer = TemplateModel(
        name=template_request.name,
        employee_count=template_request.employee_count,
        file_path=template_request.file_path,
        message=template_request.message,
        doc_start_date=template_request.doc_start_date,
        gen_start_date=template_request.gen_start_date,
        npa_date=template_request.npa_date,
        is_deleted=template_request.is_deleted,
        instruction_id=template_request.instruction_id,
        npa_name=template_request.npa_name
    )
    db.add(db_customer)
    db.commit()
    db.refresh(db_customer)

    return db_customer


async def create_upload_file(
        file: UploadFile = File(...),
        local_data=Form(...),
        db=Depends(get_session_sync)
    ):
    with cProfile.Profile() as pr:
        template = TemplateAddScheme.parse_raw(local_data)
        clean_name = clean_string(template.name)
        clean_file_name = clean_string(template.file_path)
        file_name = settings.TEMPLATE_FILES_PATH + clean_name + "_" + clean_file_name
        if os.path.exists(file_name):
            raise ValueError("Same template name")
        else:
            with open(file_name, 'wb+') as f:
                f.write(await file.read())
        template.file_path = file_name
        sql_template_model = TemplateModel()
        sql_template_model.from_pydantic(template)
        template_to_company_type = TemplateToCompanyType()
        template_to_company_type.company_type_id = template.company_type_id
        db.add(sql_template_model)
        db.flush()
        for cust_cat in template.customer_categories:
            templ_2_cust_cat = TemplateToCustomerCategory(
                customer_category_id=cust_cat,
                template_id=sql_template_model.id
            )
            db.add(templ_2_cust_cat)
        template_to_company_type.template_id = sql_template_model.id
        db.add(template_to_company_type)
        db.commit()
        db.refresh(sql_template_model)
    pr.print_stats()
    return sql_template_model


def clean_string(s):
    res = s.strip()
    res = res.replace(' ', '_')
    res = re.sub('[^A-Za-zА-Яа-я0-9_\.]', '', res)
    res = res.strip('_')
    return res
