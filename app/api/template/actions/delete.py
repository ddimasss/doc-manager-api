from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Template import TemplateModel, TemplateToCompanyType, TemplateToCustomerCategory
import os
from pydantic import BaseModel


class Item(BaseModel):
    template_id: int

async def delete(
        item: Item,
        db=Depends(get_session_sync)
):
    template_id = item.template_id
    template = db.query(TemplateModel).get(template_id)
    if template.file_path is not None and os.path.exists(template.file_path):
        os.remove(template.file_path)
    db.query(TemplateToCustomerCategory).filter(TemplateToCustomerCategory.template_id == template.id).delete()
    db.query(TemplateToCompanyType).filter(TemplateToCompanyType.template_id == template.id).delete()
    db.flush()
    db.delete(template)
    db.commit()

    return {'status': 'ok'}
