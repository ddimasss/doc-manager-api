from sqlalchemy.ext.declarative import as_declarative


@as_declarative()
class Base:
    __tablename__ = 'templates'
    __table_args__ = {'schema': 'public'}
