from fastapi import APIRouter
from .actions.add import add
from .actions.delete import edit
from .actions.get import get, get_list
from .schemas.Instructions import InstructionScheme
from typing import List


instruction_router = APIRouter()


instruction_router.add_api_route(
    path='/add',
    endpoint=add,
    methods=['POST'],
    tags=['other'],
    response_model=InstructionScheme
)
instruction_router.add_api_route(
    path='/edit',
    endpoint=edit,
    methods=['POST'],
    tags=['other'],
    response_model=InstructionScheme
)

instruction_router.add_api_route(
    path='/get',
    endpoint=get,
    methods=['GET'],
    tags=['other'],
    response_model=InstructionScheme
)

instruction_router.add_api_route(
    path='/get_list',
    endpoint=get_list,
    methods=['GET'],
    tags=['other'],
    response_model=List[InstructionScheme]
)
