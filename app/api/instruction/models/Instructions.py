from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Sequence, Date
from app.service.sqlalchemy import BaseAlch
from app.api.position.models.Position import PositionModel
from app.api.employee.models.Employee import EmployeeModel
from sqlalchemy.orm import relationship
from sqlalchemy_serializer import SerializerMixin


class InstructionModel(BaseAlch, SerializerMixin):
    __tablename__ = 'instructions'
    instruction_id_seq = Sequence('instructions_id_seq')
    id = Column(Integer, instruction_id_seq, server_default=instruction_id_seq.next_value(), primary_key=True)
    name = Column(String())
    short_name = Column(String())
    description = Column(String())
    is_deleted = Column(Boolean())


class InstructionToPosition(BaseAlch, SerializerMixin):
    __tablename__ = 'instruction_to_position'
    position_id = Column(Integer, ForeignKey(PositionModel.id), nullable=False, primary_key=True)
    instruction_id = Column(Integer, ForeignKey(InstructionModel.id), nullable=False, primary_key=True)


class InstructionToEmployee(BaseAlch, SerializerMixin):
    __tablename__ = 'instruction_to_employee'
    employee_id = Column(Integer, ForeignKey(EmployeeModel.id), nullable=False, primary_key=True)
    instruction_id = Column(Integer, ForeignKey(InstructionModel.id), nullable=False, primary_key=True)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
