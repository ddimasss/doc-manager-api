from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_INSTRUCTION_'


class InstructionSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


instruction_settings = InstructionSettings()
