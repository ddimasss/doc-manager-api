from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Instructions import InstructionModel
from ..schemas.Instructions import InstructionScheme


async def edit(
        instruction_request: InstructionScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(InstructionModel).get(instruction_request.id)
    obj.name = instruction_request.name
    obj.short_name = instruction_request.short_name
    obj.description = instruction_request.description
    obj.is_deleted = instruction_request.is_deleted
    db.commit()

    return obj
