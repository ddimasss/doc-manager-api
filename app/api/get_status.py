from app.settings import settings
from app.service.rabbitmq import producer


async def get_status():
    await producer(53, '2022-03-01', [6, ])
    return {'status': 'ok',
            'test': 'test'
        #'vertica-host': f'{settings.POSTGRES_SETTINGS.host}:{settings.POSTGRES_SETTINGS.port}',
        #'postgres-host': f'{settings.VERTICA_SETTINGS.host}:{settings.VERTICA_SETTINGS.port}'
    }
