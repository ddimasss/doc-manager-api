from fastapi import APIRouter
from .actions.get_reports import get_reports, get_reports_test


reports_general_router = APIRouter()


reports_general_router.add_api_route(
    path='/get_reports',
    endpoint=get_reports,
    methods=['GET'],
    tags=['other']
)
reports_general_router.add_api_route(
    path='/get_reports_test',
    endpoint=get_reports_test,
    methods=['GET'],
    tags=['other']
)
