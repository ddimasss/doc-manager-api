from sqlalchemy.orm import joinedload, selectinload, Session
from fastapi import Depends
from app.service.sqlalchemy import get_service_session, get_service_test_session
from sqlalchemy import select
from ..models.reports import Report
from ..models.report_groups import Module
from ..models.report_types import ReportType
from ..models.filters import Filter
from sqlalchemy.ext.asyncio import (
    AsyncSession
)



async def get_reports(
        db: AsyncSession = Depends(get_service_session)
):
    result = {}
    select_reports = select(Report)\
        .options(joinedload(Report.module))\
        .options(joinedload(Report.type))\
        .options(selectinload(Report.filters)
                 .options(selectinload(Filter.values))
                 .options(selectinload(Filter.operations))
                 )

    reports = (await db.execute(select_reports)).scalars().all()
    modules = (await db.execute(select(Module))).scalars().all()
    types = (await db.execute(select(ReportType))).scalars().all()
    result['modules'] = modules
    result['reports'] = reports
    result['types'] = types
    return result


async def get_reports_test(
        db: Session = Depends(get_service_test_session)
):
    result = {}
    select_reports = select(Report)\
        .options(joinedload(Report.module))\
        .options(joinedload(Report.type))\
        .options(selectinload(Report.filters)
                 .options(selectinload(Filter.values))
                 .options(selectinload(Filter.operations))
                 )

    reports = db.execute(select_reports).scalars().all()
    modules = db.execute(select(Module)).scalars().all()
    types = db.execute(select(ReportType)).scalars().all()
    result['modules'] = modules
    result['reports'] = reports
    result['types'] = types
    return result
