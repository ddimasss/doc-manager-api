from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from ..db import Base
from .filters import Filter


class FilterValue(Base):
    __tablename__ = 'filter_values' # noqa
    id = Column(Integer, primary_key=True)
    value_for_db = Column(String(length=255))
    value_visible = Column(String(length=255))
    filter_id = Column(Integer, ForeignKey(Filter.id), nullable=False)
    filter = relationship('Filter', back_populates='values')
