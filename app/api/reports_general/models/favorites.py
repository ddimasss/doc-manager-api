from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from ..db import Base
from .reports import Report


class Favorite(Base):
    __tablename__ = 'favorites' # noqa
    id = Column(Integer, primary_key=True)
    msp = Column(String(length=255))
    ou = Column(String(length=255))
    user_name = Column(String(length=255))
    description = Column(String)
    report_id = Column(Integer, ForeignKey(Report.id), nullable=False)
    #report = relationship('Report', back_populates='favorites')
