from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from ..db import Base
from .reports import Report


class Filter(Base):
    __tablename__ = 'filters' # noqa
    id = Column(Integer, primary_key=True)
    type = Column(String(length=255))
    name = Column(String(length=255))
    name_eng = Column(String(length=255))
    description = Column(String)
    fa_icon_name = Column(String(length=40))
    report_id = Column(Integer, ForeignKey(Report.id), nullable=False)
    report = relationship('Report', back_populates='filters')
    operations = relationship('FilterOp')
    values = relationship('FilterValue')
