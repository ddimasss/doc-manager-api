from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from ..db import Base
from .filters import Filter


class FilterOp(Base):
    __tablename__ = 'filter_operations' # noqa
    id = Column(Integer, primary_key=True)
    operation = Column(String(length=255))
    filter_id = Column(Integer, ForeignKey(Filter.id), nullable=False)
    filter = relationship('Filter', back_populates='operations')
