from pydantic import BaseModel, Field
from enum import Enum
from datetime import date
from typing import List, Optional, Any


class OkvedScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    name: str
    okved: str
    description: Optional[str] = None
    is_deleted: Optional[bool] = False
