from fastapi import APIRouter
from .actions.add import add
from .actions.delete import edit
from .actions.get import get, get_list
from .schemas.Okved import OkvedScheme
from typing import List


okved_router = APIRouter()


okved_router.add_api_route(
    path='/add',
    endpoint=add,
    methods=['POST'],
    tags=['other'],
    response_model=OkvedScheme
)
okved_router.add_api_route(
    path='/edit',
    endpoint=edit,
    methods=['POST'],
    tags=['other'],
    response_model=OkvedScheme
)

okved_router.add_api_route(
    path='/get',
    endpoint=get,
    methods=['GET'],
    tags=['other'],
    response_model=OkvedScheme
)

okved_router.add_api_route(
    path='/get_list',
    endpoint=get_list,
    methods=['GET'],
    tags=['other'],
    response_model=List[OkvedScheme]
)
