from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship
from app.service.sqlalchemy import BaseAlch


class OkvedModel(BaseAlch):
    __tablename__ = 'okveds'
    id = Column(Integer, primary_key=True)
    name = Column(String())
    okved = Column(String())
    description = Column(String())
    is_deleted = Column(Boolean())
    # Customers = relationship("CustomerModel")
