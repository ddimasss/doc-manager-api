import logging
from typing import cast

from fastapi import Depends, Request, Body
from app.service.sqlalchemy import get_session_sync
from ..models.Customer import CustomerModel, Address, CustomersToOkved, CustomersToCustomerCategory
from app.api.position.models.Position import PositionModel, position_from_dict
from app.api.employee.models.Employee import EmployeeModel
from app.api.employee.models.EmployeeToPosition import EmployeeToPositionsModel
from ..schemas.Customer import CustomerSchemeIn
import orjson
import sqlalchemy
from sqlalchemy.exc import DatabaseError
from app.generator.main import generate_for_staff_update
from sqlalchemy.orm import Session

logger = logging.getLogger('gunicorn')


async def delete(
        customer_id: int = Body(),
        db=Depends(get_session_sync)
):
    obj = cast(CustomerModel, db.query(CustomerModel).get(customer_id))
    obj.is_deleted = True
    try:
        db.commit()
    except DatabaseError:
        return {'result': 'error', 'message': 'Не могу удалить объект'}
    return {'result': 'ok'}


async def update_customer(
        body: Request,
        db=Depends(get_session_sync)
    ):
    req = orjson.loads(await body.body())
    try:
        # with open('/opt/app/log.txt', 'w') as file:
        #     file.write(str(req))
        customer = db.query(CustomerModel).get(req['main']['id'])
        customer.itn = req['main']['itn']
        customer.company_type_id = req['main']['company_type_id']
        customer.full_name = req['main']['full_name']
        customer.time_zone = req['main']['timezone']
        customer.contract_date_start = req['main2']['contract_date_start']
        customer.contract_date_end = req['main2']['contract_date_end']
        customer.contract_number = req['main2']['contract_number']
        customer.short_name = req['main']['short_name']
        customer.ogrn = req['main']['ogrn']
        customer.kpp = req['main']['kpp']
        customer.phone = req['main']['phone']
        customer.email = req['main']['email']
        customer.is_deleted = req['main2']['is_deleted'] if 'is_deleted' in req['main2'] else False
        customer.contact_name = req['main2']['contact_name'] if 'contact_name' in req['main2'] else None
        customer.contact_phone = req['main2']['contact_phone'] if 'contact_phone' in req['main2'] else None
        customer.contact_email = req['main2']['contact_email'] if 'contact_email' in req['main2'] else None
        customer.description = req['main2']['description'] if 'description' in req['main2'] else None
        customer.ros_fin_login = req['main2']['rosFinLogin'] if 'rosFinLogin' in req['main2'] else None
        customer.ros_fin_pass = req['main2']['rosFinPass'] if 'rosFinPass' in req['main2'] else None
        customer.tariff_id = req['main2']['tariff_id']
        db.merge(customer)
        db.flush()
        for addr in req['main']['addresses']:
            if 'action' in addr:
                if addr['action'] == 'Update':
                    address = db.query(Address).get(addr['id'])
                    address.address = addr['address']
                    address.date_start = addr['date_start']
                    address.date_end = addr['date_end'] if 'date_end' in addr else None
                    address.is_deleted = False
                    address.type = addr['type']
                    address.customer_id = customer.id
                    db.merge(address)
                if addr['action'] == 'Add':
                    address = Address(
                        address=addr['address'],
                        date_start=addr['date_start'],
                        date_end=addr['date_end'] if 'date_end' in addr else None,
                        is_deleted=False,
                        type=addr['type'],
                        customer_id=customer.id
                    )
                    db.add(address)
                if addr['action'] == 'Delete':
                    address = db.query(Address).get(addr['id'])
                    db.delete(address)

        # db.add(actual_address)
        # mailing_address = Address(
        #     address=req['main']['poc_address'],
        #     date_start=req['main']['start_date_poc'],
        #     date_end=req['main'] if 'end_date_poc' in req['main'] else None,
        #     is_deleted=False,
        #     type='MAILING',
        #     customer_id=db_customer.id
        # )
        # db.add(mailing_address)
        db.query(CustomersToCustomerCategory).filter(CustomersToCustomerCategory.customer_id == customer.id).delete()
        db.flush()

        for category_id in req['main']['customer_categories']:
            print(category_id)
            customer_to_customer_category = CustomersToCustomerCategory(
                customer_id=customer.id,
                customer_category_id=category_id
            )
            db.merge(customer_to_customer_category)

        for okved in req['mainOkveds']:
            if 'action' in okved and okved['action'] == 'Delete':
                cust_to_okved = db.query(CustomersToOkved).get((okved['customer_id'], okved['okved_id']))
                db.delete(cust_to_okved)
            else:
                link_to_okved = CustomersToOkved(
                    customer_id=customer.id,
                    okved_id=okved['okved_id'],
                    type='main',
                    date_start=okved['date_start'],
                    date_end=okved['date_end'] if 'date_end' in okved and okved['date_end']!='' else None
                )
                db.merge(link_to_okved)
        for okved in req['otherOkveds']:
            if 'action' in okved and okved['action'] == 'Delete':
                cust_to_okved = db.query(CustomersToOkved).get((okved['customer_id'], okved['okved_id']))
                db.delete(cust_to_okved)
            else:
                link_to_okved = CustomersToOkved(
                    customer_id=customer.id,
                    okved_id=okved['okved_id'],
                    type='other',
                    date_start=okved['date_start'],
                    date_end=okved['date_end'] if 'date_end' in okved and okved['date_end']!= '' else None
                )
                db.merge(link_to_okved)
        db.flush()
        links = {}
        for staffstruct in req['staffstruct']:
            if 'action' in staffstruct:
                if staffstruct['action'] == 'Add':
                    position = position_from_dict(staffstruct, staffstruct['action'], customer.id)
                    db.add(position)
                    links[staffstruct['id']] = position.id
                if staffstruct['action'] == 'Update':
                    position = db.query(PositionModel).get(staffstruct['id'])
                    position_from_dict(staffstruct, staffstruct['action'], customer.id, position)
                    db.merge(position)
                    links[position.id] = position.id
                if staffstruct['action'] == 'Delete':
                    position = db.query(PositionModel).get(staffstruct['id'])
                    db.delete(position)
            else:
                links[staffstruct['id']] = staffstruct['id']
        db.flush()
        for staff in req['staff']:
            if 'action' in staff:
                if staff['action'] == 'Add':
                    employee = EmployeeModel(
                        f_name=staff['f_name'],
                        l_name=staff['l_name_i'],
                        s_name=staff['s_name'],
                        l_name_r=staff['l_name_r'],
                        l_name_d=staff['l_name_d'],
                        l_name_v=staff['l_name_v'],
                        l_name_t=staff['l_name_t'],
                        customer_id=customer.id,
                        start_date=staff['start_date'],
                        end_date=staff['end_date'] if 'end_date' in staff and staff['end_date']!='' else None,
                        ci_end_date=staff['ci_end_date'] if 'ci_end_date' in staff and staff['ci_end_date']!='' else None,
                        sdl_start_date=staff['sdl_start_date'] if 'sdl_start_date' in staff and staff['sdl_start_date']!='' else None
                    )
                    db.add(employee)
                    db.flush()
                    employee_to_position = EmployeeToPositionsModel(
                        employee_id=employee.id,
                        position_id=links[staff['position_id']],
                        start_date=staff['start_date'],
                        end_date=staff['end_date'] if 'end_date' in staff and staff['end_date']!='' else None
                    )
                    db.add(employee_to_position)
                if staff['action'] == 'Update':
                    employee = db.query(EmployeeModel).get(staff['id'])
                    employee.f_name = staff['f_name']
                    employee.l_name = staff['l_name_i']
                    employee.s_name = staff['s_name']
                    employee.l_name_r = staff['l_name_r']
                    employee.l_name_d = staff['l_name_d']
                    employee.l_name_v = staff['l_name_v']
                    employee.l_name_t = staff['l_name_t']
                    employee.customer_id = customer.id
                    employee.start_date = staff['start_date']
                    employee.end_date = staff['end_date'] if 'end_date' in staff and staff['end_date']!='' else None
                    employee.ci_end_date = staff['ci_end_date'] if 'ci_end_date' in staff and staff['ci_end_date']!='' else None
                    employee.sdl_start_date = staff['sdl_start_date'] if 'sdl_start_date' in staff and staff['sdl_start_date']!='' else None
                    db.merge(employee)
                    db.flush()
                    employee_to_position = db.query(EmployeeToPositionsModel).get((staff['id'], staff['position_id']))
                    employee_to_position.position_id = links[staff['position_id']],
                    employee_to_position.start_date = staff['start_date'],
                    employee_to_position.end_date = staff['end_date'] if 'end_date' in staff and staff['end_date'] != '' else None
                    db.merge(employee_to_position)
                    db.flush()
                if staff['action'] == 'Delete':
                    employee = db.query(EmployeeModel).get(staff['id'])
                    employee_to_position = db.query(EmployeeToPositionsModel).get(
                        (staff['id'], staff['position_id']))
                    db.delete(employee_to_position)
                    db.flush()
                    db.delete(employee)
            db.commit()
    except DatabaseError:
        return {'result': 'error', 'message': 'Существуют связи с удаляемым объектом.'}

    return {'result': 'ok'}
