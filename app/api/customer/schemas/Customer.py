from pydantic import BaseModel, Field
from enum import Enum
from datetime import date
from typing import List, Optional, Any
from datetime import date
from app.api.customer_category.schemas.CustomerCategory import CustomerCategoryScheme


class CustomerSchemeGen(BaseModel):
    class Config:
        from_attributes = True
    id: int
    date: date
    instruction_ids: List[int]
    user_id: int


class CustomerSchemeIn(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    itn: str
    company_type_id: int
    full_name: str
    time_zone: int
    contract_date_start: date
    contract_date_end: date
    contract_number: str
    short_name: str
    ogrn: Optional[str]
    kpp: Optional[str]
    phone: Optional[str]
    is_deleted: Optional[bool] = False
    description: Optional[str] = None
    tariff_id: int
    contact_name: Optional[str]
    contact_phone: Optional[str]
    contact_email: Optional[str]
    description: Optional[str]
    ros_fin_login: Optional[str]
    ros_fin_pass: Optional[str]
    customer_category_id: Optional[List[int]]


class CustomerSchemeOut(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    itn: str
    company_type_id: int
    full_name: str
    time_zone: int
    contract_date_start: date
    contract_date_end: date
    contract_number: str
    short_name: str
    ogrn: Optional[str]
    kpp: Optional[str]
    phone: Optional[str]
    email: Optional[str]
    is_deleted: Optional[bool] = False
    description: Optional[str] = None
    tariff_id: int
    contact_name: str
    contact_phone: str
    customer_category: List[CustomerCategoryScheme]
