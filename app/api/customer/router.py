from fastapi import APIRouter
from .actions.add import create_customer
from .actions.generate import gen, gen_many
from .actions.edit import update_customer, delete
from .actions.get import get, get_list, get_staff_struct_by_id, get_all_by_customer_id
from .schemas.Customer import CustomerSchemeIn, CustomerSchemeGen
from app.api.position.schemas.Position import PositionScheme
from typing import List, Any

customer_router = APIRouter()

customer_router.add_api_route(
    path='/generate',
    endpoint=gen,
    methods=['POST'],
    tags=['other']
)
customer_router.add_api_route(
    path='/generate_many',
    endpoint=gen_many,
    methods=['POST'],
    tags=['other']
)

customer_router.add_api_route(
    path='/create',
    endpoint=create_customer,
    methods=['POST'],
    tags=['other']
)
customer_router.add_api_route(
    path='/update',
    endpoint=update_customer,
    methods=['POST'],
    tags=['other']
)
customer_router.add_api_route(
    path='/delete',
    endpoint=delete,
    methods=['POST'],
    tags=['other']
)

customer_router.add_api_route(
    path='/get/{customer_id}',
    endpoint=get,
    methods=['GET'],
    tags=['other'],
    response_model=CustomerSchemeIn
)
customer_router.add_api_route(
    path='/get_all/{customer_id}',
    endpoint=get_all_by_customer_id,
    methods=['GET'],
    tags=['other']
)

customer_router.add_api_route(
    path='/get_stuff_struct/{customer_id}',
    endpoint=get_staff_struct_by_id,
    methods=['GET'],
    tags=['other'],
    response_model=List[PositionScheme]
)


customer_router.add_api_route(
    path='/get_list',
    endpoint=get_list,
    methods=['GET'],
    tags=['other'],
    #response_model=List[Any]
)
