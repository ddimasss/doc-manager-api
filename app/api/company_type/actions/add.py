from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from sqlalchemy import select
from ..models.CompanyType import CompanyTypeSqlA
from ..schemas.CompanyType import CompanyType


async def add(
        company_type_request: CompanyType,
        db=Depends(get_session_sync)
):
    db_company_type = CompanyTypeSqlA(
        full_name=company_type_request.full_name,
        short_name=company_type_request.short_name,
        description=company_type_request.description,
        is_deleted=0
    )
    db.add(db_company_type)
    db.commit()
    db.refresh(db_company_type)

    return db_company_type
