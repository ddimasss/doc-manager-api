from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from sqlalchemy import select
from ..models.CompanyType import CompanyTypeSqlA
from ..schemas.CompanyType import CompanyType


async def edit(
        company_type_request: CompanyType,
        db=Depends(get_session_sync)
):
    obj = db.query(CompanyTypeSqlA).get(company_type_request.id)
    obj.full_name = company_type_request.full_name
    obj.short_name = company_type_request.short_name
    obj.description = company_type_request.description
    obj.is_deleted = company_type_request.is_deleted
    db.commit()

    return obj
