from fastapi import APIRouter
from .actions.add import add
from .actions.edit import edit
from .actions.get import get, get_list
from .schemas.CompanyType import CompanyType
from typing import List


company_type_router = APIRouter()


company_type_router.add_api_route(
    path='/add',
    endpoint=add,
    methods=['POST'],
    tags=['other'],
    response_model=CompanyType
)

company_type_router.add_api_route(
    path='/edit',
    endpoint=edit,
    methods=['POST'],
    tags=['other'],
    response_model=CompanyType
)

company_type_router.add_api_route(
    path='/get',
    endpoint=get,
    methods=['GET'],
    tags=['other'],
    response_model=CompanyType
)

company_type_router.add_api_route(
    path='/get_list',
    endpoint=get_list,
    methods=['GET'],
    tags=['other'],
    response_model=List[CompanyType]
)
