from sqlalchemy import Column, Integer, String, Boolean
from app.service.sqlalchemy import BaseAlch
from ..schemas.CompanyType import CompanyType

class CompanyTypeSqlA(BaseAlch):
    __tablename__ = 'company_types'
    id = Column(Integer, primary_key=True)
    full_name = Column(String())
    short_name = Column(String())
    description = Column(String())
    is_deleted = Column(Boolean())

    def from_pydantic(self, scheme: CompanyType):
        public_names = [
            "id",
            "full_name",
            "short_name",
            "description",
            "is_deleted"
        ]
        for name in public_names:
            attr = getattr(scheme, name, None)
            if attr is not None:
                setattr(self, name, attr)
