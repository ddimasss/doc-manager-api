from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from ..models.Settings import SettingsModel
from ..schemas.Settings import SettingsScheme
import logging


async def add(
        settings_request: SettingsScheme,
        db=Depends(get_session_sync)
):
    if 'id' in settings_request:
        del settings_request.id
    db_settings = SettingsModel(
        setting_name=settings_request.setting_name,
        setting_value=settings_request.setting_value
    )
    db.add(db_settings)
    db.commit()
    db.refresh(db_settings)

    return db_settings
