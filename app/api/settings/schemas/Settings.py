from pydantic import BaseModel
from typing import Optional


class SettingsScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    setting_name: str
    setting_value: Optional[str]
