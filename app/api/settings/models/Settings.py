from sqlalchemy import Column, Integer, String, Sequence
from app.service.sqlalchemy import BaseAlch


class SettingsModel(BaseAlch):
    __tablename__ = 'settings'
    settings_id_seq = Sequence('settings_id_seq')
    id = Column(Integer, settings_id_seq, server_default=settings_id_seq.next_value(), primary_key=True)
    setting_name = Column(String, nullable=False)
    setting_value = Column(String, nullable=True)
