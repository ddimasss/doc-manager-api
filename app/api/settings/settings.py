from pydantic_settings import BaseSettings
ENV_PREFIX = 'DMA_SETTINGS_'


class SettingsSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


settings_settings = SettingsSettings()