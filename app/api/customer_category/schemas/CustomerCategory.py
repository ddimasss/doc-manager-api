from pydantic import BaseModel
from typing import List, Optional


class CustomerCategoryScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    name: str
    description: Optional[str] = None
    is_deleted: Optional[bool] = False
