from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from app.service.sqlalchemy import BaseAlch
from app.api.okved.models.Okved import OkvedModel


class CustomerCategoryModel(BaseAlch):
    __tablename__ = 'customer_category'
    id = Column(Integer, primary_key=True)
    name = Column(String())
    description = Column(String())
    is_deleted = Column(Boolean())


class CustomersCategoryToOkveds(BaseAlch):
    __tablename__ = 'customer_category_to_okveds'
    okved_id = Column(Integer, ForeignKey(OkvedModel.id), nullable=False, primary_key=True)
    customer_category_id = Column(Integer, ForeignKey(CustomerCategoryModel.id), nullable=False, primary_key=True)

