from sqlalchemy.ext.declarative import as_declarative


@as_declarative()
class Base:
    __tablename__ = 'customer_category'
    __table_args__ = {'schema': 'public'}
