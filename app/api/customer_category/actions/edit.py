from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from sqlalchemy import select
from ..models.CustomerCategory import CustomerCategoryModel
from ..schemas.CustomerCategory import CustomerCategoryScheme


async def edit(
        customer_category_request: CustomerCategoryScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(CustomerCategoryModel).get(customer_category_request.id)
    obj.name = customer_category_request.name
    obj.description = customer_category_request.description
    obj.is_deleted = customer_category_request.is_deleted
    db.commit()

    return obj
