from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.CustomerCategory import CustomerCategoryModel
from ..schemas.CustomerCategory import CustomerCategoryScheme


async def add(
        company_type_request: CustomerCategoryScheme,
        db=Depends(get_session_sync)
):
    db_customer_category = CustomerCategoryModel(
        name=company_type_request.name,
        description=company_type_request.description,
        is_deleted=0
    )
    db.add(db_customer_category)
    db.commit()
    db.refresh(db_customer_category)

    return db_customer_category
