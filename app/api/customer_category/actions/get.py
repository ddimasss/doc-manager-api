from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.CustomerCategory import CustomerCategoryModel


async def get(
        company_type_id: int,
        db=Depends(get_session_sync)
):
    obj = db.query(CustomerCategoryModel).get(company_type_id)
    db.commit()

    return obj


async def get_list(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    obj = db.query(CustomerCategoryModel).limit(limit).offset(offset).all()
    res = obj

    return res
