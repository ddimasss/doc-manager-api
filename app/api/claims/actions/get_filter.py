from fastapi import Depends
from app.service.sqlalchemy import get_session
from sqlalchemy import select, distinct
from ..models.claims_subject import ClaimSubject
from ..models.claim import Claim
from ..settings import claim_settings
from sqlalchemy.ext.asyncio import (
    AsyncSession
)


async def get_filter(
        db: AsyncSession = Depends(get_session)
):
    result = {}

    select_subjects = select(ClaimSubject.subject_name.label("label"), ClaimSubject.subject_name.label("value"))
    result['subject'] = (await db.execute(select_subjects)).all()

    select_claim_statuses = select(distinct(Claim.status.label("label")), Claim.status.label("value"))
    result['claim_statuses'] = (await db.execute(select_claim_statuses)).all()

    select_insurer = select(distinct(Claim.insurance_company_msp).label('value'), Claim.insurance_company.label('label'))
    result['insurance_companies'] = (await db.execute(select_insurer)).all()

    select_currency = select(distinct(Claim.currency_code).label('value'), Claim.currency.label('label'))
    result['currencies'] = (await db.execute(select_currency)).all()

    result['insurance_types'] = claim_settings.INSURANCE_TYPES
    result['vip_statuses'] = claim_settings.VIP_STATUS
    result['aggregate_levels'] = claim_settings.AGGREGATE_LEVELS

    return result
