from typing import Any
from sqlalchemy.ext.declarative import as_declarative


@as_declarative()
class Base:
    __tablename__ = 'claims'
    __table_args__ = {'schema': 'arrangement'}
