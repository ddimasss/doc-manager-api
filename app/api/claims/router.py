from fastapi import APIRouter
from .actions.get_claims import get_claims, get_claims_test
from .actions.get_filter import get_filter


claims_router = APIRouter()


claims_router.add_api_route(
    path='/get_claims',
    endpoint=get_claims,
    methods=['POST'],
    tags=['other']
)

claims_router.add_api_route(
    path='/get_claims_test',
    endpoint=get_claims_test,
    methods=['POST'],
    tags=['other']
)

claims_router.add_api_route(
    path='/get_filter',
    endpoint=get_filter,
    methods=['GET'],
    tags=['other']
)
