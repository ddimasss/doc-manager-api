from sqlalchemy import Column, Integer, ForeignKey
from ..db import Base
from .claim import Claim


class ClaimSubjectLink(Base):
    __tablename__ = 'claims_subject_link'  # noqa
    claim_id = Column(Integer, ForeignKey(Claim.claimid), nullable=False, primary_key=True)
    subject_id = Column(Integer, primary_key=True)
