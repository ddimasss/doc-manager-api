from sqlalchemy import Column, Integer, String
from ..db import Base


class ClaimSubject(Base):
    __tablename__ = 'claims_subject'  # noqa
    subject_id = Column(Integer, primary_key=True)
    subject_name = Column(String(length=20))
