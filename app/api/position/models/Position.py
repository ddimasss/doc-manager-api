from sqlalchemy import Column, Integer, String, Date, Sequence, ForeignKey, Boolean
from app.service.sqlalchemy import BaseAlch


class PositionModel(BaseAlch):
    __tablename__ = 'positions'
    positions_id_seq = Sequence('positions_id_seq')
    id = Column(Integer, positions_id_seq, server_default=positions_id_seq.next_value(), primary_key=True)
    name_r = Column(String, nullable=False)
    name_i = Column(String, nullable=False)
    name_d = Column(String, nullable=False)
    name_v = Column(String, nullable=False)
    name_t = Column(String, nullable=False)
    description = Column(String, nullable=True)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
    customer_id = Column(Integer, ForeignKey('public.customers.id'), nullable=False)
    seo = Column(Boolean, nullable=False)
    sdl = Column(Boolean, nullable=False)


def position_from_dict(pos: dict, action: str, customer_id: int, position: PositionModel = None) -> PositionModel:
    if action == 'Add':
        position = PositionModel(
            name_r=pos['name_r'],
            name_i=pos['name_i'],
            name_d=pos['name_d'],
            name_v=pos['name_v'],
            name_t=pos['name_t'],
            description=pos['description'] if 'description' in pos else None,
            start_date=pos['start_date'],
            end_date=pos['end_date'] if (
                        ('end_date' in pos) and (pos['end_date'] != '')) else None,
            customer_id=customer_id,
            seo=pos['seo'] if 'seo' in pos and pos['seo'] != '' else False,
            sdl=pos['sdl'] if 'sdl' in pos and pos['sdl'] != '' else False,
        )
    if action == 'Update':
        position.name_r = pos['name_r']
        position.name_i = pos['name_i']
        position.name_d = pos['name_d']
        position.name_v = pos['name_v']
        position.name_t = pos['name_t']
        position.description = pos['description'] if 'description' in pos else None
        position.start_date = pos['start_date']
        position.end_date = pos['end_date'] if (
                ('end_date' in pos) and (pos['end_date'] != '')) else None
        position.customer_id = customer_id,
        position.seo = pos['seo'] if 'seo' in pos and pos['seo'] != '' else False
        position.sdl = pos['sdl'] if 'sdl' in pos and pos['sdl'] != '' else False

    return position
