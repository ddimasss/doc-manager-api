from sqlalchemy import Column, Integer, ForeignKey, Date, Sequence
from app.service.sqlalchemy import BaseAlch
from app.api.position.models.Position import PositionModel


class CeoModel(BaseAlch):
    __tablename__ = 'ceo'
    ceo_id_seq = Sequence('ceo_id_seq')
    id = Column(Integer, ceo_id_seq, server_default=ceo_id_seq.next_value(), primary_key=True)
    position_id = Column(Integer, ForeignKey(PositionModel.id), nullable=False)
    start_date = Column(Date, nullable=True)
    end_date = Column(Date, nullable=True)