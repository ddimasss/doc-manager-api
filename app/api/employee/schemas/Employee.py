from pydantic import BaseModel, Field
from typing import List, Optional, Any
from datetime import date


class EmployeeScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    f_name: str
    l_name: str
    s_name: Optional[str]
    l_name_r: str
    l_name_d: str
    l_name_v: str
    l_name_t: str
    customer_id: int
    start_date: date
    end_date: Optional[date]
