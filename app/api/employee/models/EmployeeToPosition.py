from app.service.sqlalchemy import BaseAlch
from sqlalchemy import Column, Integer, Date, ForeignKey


class EmployeeToPositionsModel(BaseAlch):
    __tablename__ = 'employees_to_positions'
    employee_id = Column(Integer, ForeignKey('public.employees.id'), primary_key=True, nullable=False)
    position_id = Column(Integer, ForeignKey('public.customers.id'), primary_key=True, nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
