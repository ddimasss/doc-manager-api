from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from ..models.Employee import EmployeeModel
from ..models.EmployeeToPosition import EmployeeToPositionsModel
from ..schemas.Employee import EmployeeScheme
from app.generator.main import generate_for_employee_change
import logging
from datetime import datetime


async def add(
        employee_request: EmployeeScheme,
        db=Depends(get_session_sync)
):
    logging.error(employee_request)
    if 'id' in employee_request:
        del employee_request.id
    db_employee = EmployeeModel(
        f_name=employee_request.f_name,
        l_name=employee_request.l_name,
        s_name=employee_request.s_name,
        l_name_r=employee_request.l_name_r,
        l_name_d=employee_request.l_name_d,
        l_name_v=employee_request.l_name_v,
        l_name_t=employee_request.l_name_t,
        customer_id=employee_request.customer_id,
        start_date=employee_request.start_date,
        end_date=employee_request.end_date
    )

    db.add(db_employee)

    db.commit()
    db.refresh(db_employee)
    emp_2_pos = EmployeeToPositionsModel(
        employee_id=db_employee.id,
        position_id=db_employee.customer_id,
        start_date=datetime.now(),
        end_date=None
    )
    db.add(emp_2_pos)
    db.commit()
    generate_for_employee_change(date_for_generate=datetime.now(),
                                 customer_id=db_employee.customer_id,
                                 employee_id=db_employee.id
                                 )

    return db_employee
