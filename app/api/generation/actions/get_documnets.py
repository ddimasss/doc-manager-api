from sqlalchemy.orm import Session
from fastapi import Depends
from app.service.sqlalchemy import get_session
from sqlalchemy import select
from ..schemas.generation import GenRequest
from sqlalchemy.sql import func
from sqlalchemy.ext.asyncio import (
    AsyncSession
)
from app.generator.main import get_from_db

async def get_docs(
        gen_request: GenRequest,
        db: AsyncSession = Depends(get_session)
):
    dt = gen_request.date_start
    records = get_from_db(date_for_generation=dt, customer_id=gen_request.customer_id,
                                   gen_start_date=False, types=gen_request.document_types)

    return records
