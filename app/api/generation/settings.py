from typing import Set
from pydantic import (
    BaseSettings,
    BaseModel
)

ENV_PREFIX = 'REP_CLAIM_'


class InsuranceType(BaseModel):
    cargo = 'Грузы'
    unknown = 'Неизвестно'
    delay = 'Сроки'


class AggregateLevels(BaseModel):
    status = 'Статус претензии'
    insurance_type = 'Тип страхования'
    subject = 'Предмет обращения'


class ClaimSettings(BaseSettings):
    INSURANCE_TYPES = [
        {
            'value': 'cargo',
            'label': 'Грузы'
        },
        {
            'value': 'delay',
            'label': 'Сроки'
        },
        {
            'value': 'unknown',
            'label': 'Неизвестно'
        }
    ]
    AGGREGATE_LEVELS = [
        {
            'value': 'status',
            'label': 'Статус претензии'
        },
        {
            'value': 'insurance_type',
            'label': 'Тип страхования'
        }
        ,
        {
            'value': 'subject',
            'label': 'Предмет обращения'
        }
    ]
    VIP_STATUS = [
        {
            'value': 'Все',
            'label': 'Все'
        },
        {
            'value': 'Да',
            'label': 'Да'
        }
        ,
        {
            'value': 'Нет',
            'label': 'Нет'
        }
    ]

    class Config:
        env_prefix = ENV_PREFIX


claim_settings = ClaimSettings()