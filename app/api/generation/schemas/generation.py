from pydantic import BaseModel
from datetime import date
from typing import List


class GenRequest(BaseModel):
    class Config:
        from_attributes = True
    customer_id: int
    date_start: date
    date_end: date
    document_types: List[int]
