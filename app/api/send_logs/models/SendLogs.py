from sqlalchemy import String, Column, Integer, ForeignKey, Date, DateTime, Sequence
from app.service.sqlalchemy import BaseAlch
from sqlalchemy.orm import relationship
from sqlalchemy_serializer import SerializerMixin


class SendLogsModel(BaseAlch, SerializerMixin):
    __tablename__ = 'sending_logs'
    send_logs__id_seq = Sequence('sending_logs_id_seq')
    id = Column(Integer, send_logs__id_seq, server_default=send_logs__id_seq.next_value(), primary_key=True)
    user_id = Column(Integer, ForeignKey('public.users.id'), nullable=False)
    user = relationship('UserModel', lazy="joined")
    created_at = Column(DateTime, nullable=False)
    customer_id = Column(Integer, ForeignKey('public.customers.id'), nullable=False)
    customer = relationship('CustomerModel', lazy="joined")
    date = Column(Date, nullable=False)
    instruction_id = Column(Integer, ForeignKey('public.instructions.id'), nullable=False)
    instruction = relationship('InstructionModel', lazy="joined")
    status = Column(String, nullable=False)
