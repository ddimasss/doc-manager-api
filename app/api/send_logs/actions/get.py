from typing import cast

from fastapi import Depends
from fastapi import Response

from app.middleware.UserActionsToDB import UserActionsLogsModel
from app.service.sqlalchemy import get_session_sync
from app.service.xlsx import make_xlsx
from ..models.SendLogs import SendLogsModel
from sqlalchemy.orm import joinedload

from ...customer.models.Customer import CustomerModel
from ...instruction.models.Instructions import InstructionModel
from ...users.models.Users import UserModel


async def get(
        log_id: int,
        db=Depends(get_session_sync)
):
    obj = db.query(SendLogsModel).get(log_id)
    db.commit()

    return obj


async def get_list(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    obj = db.query(SendLogsModel)\
        .options(joinedload(SendLogsModel.user)) \
        .options(joinedload(SendLogsModel.customer)) \
        .options(joinedload(SendLogsModel.instruction)) \
        .order_by(SendLogsModel.id.desc()) \
        .limit(limit).offset(offset).all()
    res = [row.to_dict() for row in obj]

    return res


async def get_xlsx(
        db=Depends(get_session_sync)
):
    objects = db.query(SendLogsModel)\
        .options(joinedload(SendLogsModel.user)) \
        .options(joinedload(SendLogsModel.customer)) \
        .options(joinedload(SendLogsModel.instruction)) \
        .order_by(SendLogsModel.id.desc()) \
        .all()
    rows = []
    xlsx_head = ['Время создания', 'Имя пользователя', 'Фамилия пользователя', 'Клиент', 'ИНН', 'Тип шаблона', 'Статус']
    rows.append(xlsx_head)
    for log in objects:
        log = cast(SendLogsModel, log)
        user = cast(UserModel, log.user)
        customer = cast(CustomerModel, log.customer)
        instruction = cast(InstructionModel, log.instruction)
        rows.append([log.created_at.isoformat(), user.first_name, user.last_name, customer.short_name, customer.itn, instruction.name, log.status])
    res = make_xlsx(rows, "Logs")

    headers = {
        'Content-Disposition': 'attachment; filename="logs.xlsx"'
    }
    return Response(res, headers=headers)


async def get_list_user_actions(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    obj = db.query(UserActionsLogsModel)\
        .order_by(UserActionsLogsModel.id.desc()) \
        .limit(limit).offset(offset).all()
    res = [row.to_dict() for row in obj]

    return res


async def get_xlsx_user_actions(
        db=Depends(get_session_sync)
):
    objects = db.query(UserActionsLogsModel) \
        .order_by(UserActionsLogsModel.id.desc()) \
        .all()
    rows = []
    xlsx_head = ['Пользователь', 'Время', 'Действие']
    rows.append(xlsx_head)
    for action in objects:
        path_wo_prefix = action.endpoint[9:];
        path_arr = path_wo_prefix.split('/');
        path = path_arr[0] + '/' + path_arr[1];
        rows.append([action.user_email, action.created_at, paths.get(path, '')])
    res = make_xlsx(rows, "User_actions")

    headers = {
        'Content-Disposition': 'attachment; filename="logs.xlsx"'
    }
    return Response(res, headers=headers)


paths = {
  'tariff/get_list': 'Просмотр тарифов',
  'gen/get_documents': 'Просмотр документов доступных для генерации',
  'company_type/get_list': 'Просмотр списка типов организаций',
  'company_type/get': 'Просмотр организации',
  'company_type/edit': 'Редактирование организации',
  'company_type/add': 'Добавление организации',
  'okved/get_list': 'Просмотр списка ОКВЭД',
  'okved/get': 'Просмотр ОКВЭД',
  'okved/edit': 'Редактирование ОКВЭД',
  'okved/add': 'Добавление ОКВЭД',
  'message/get_list': 'Просмотр списка сообщений',
  'message/get': 'Просмотр сообщения',
  'message/add': 'Добавление сообщения',
  'message/edit': 'Редактирование сообщения',
  'customer/get_list': 'Просмотр списка клиентов',
  'customer/get': 'Просмотр клиента',
  'customer/get_all': 'Просмотр всех данных клиента',
  'customer/get_stuff_struct': 'Просмотр организационной структуры клиента',
  'customer/create': 'Добавление клиента',
  'customer/delete': 'Удаление клиента',
  'customer/update': 'Редактирование клиента',
  'customer/generate': 'Генерация документов для клиента',
  'customer/generate_many': 'Генерация документов для списка клиентов',
  'employee/get_list': 'Просмотр списка сотрудников',
  'employee/get': 'Просмотр сотрудника',
  'employee/add': 'Добавление сотрудника',
  'employee/edit': 'Редактирование сотрудника',
  'instruction/get_list': 'Просмотр списка документов',
  'instruction/get': 'Просмотр документа',
  'instruction/add': 'Добавление документа',
  'instruction/edit': 'Редактирование документа',
  'position/get_list': 'Просмотр списка должностей',
  'position/get': 'Просмотр должности',
  'position/add': 'Добавление должности',
  'position/edit': 'Редактирование должности',
  'settings/get_list': 'Просмотр списка настроек',
  'settings/get': 'Просмотр настройки',
  'settings/add': 'Добавление настройки',
  'settings/edit': 'Редактирование настройки',
  'templates/get_list': 'Просмотр списка шаблонов',
  'templates/get': 'Просмотр шаблона',
  'templates/add': 'Добавление шаблона',
  'templates/edit': 'Редактирование шаблона',
  'templates/delete': 'Удаление шаблона',
  'templates/upload': 'Добавление шаблона',
  'templates/edit_upload': 'Редактирование шаблона',
  'customer_category/get_list': 'Просмотр списка категорий клиентов',
  'customer_category/get': 'Просмотр категории клиентов',
  'customer_category/add': 'Добавление категории клиентов',
  'customer_category/edit': 'Редактирование категории клиентов',
  'send_logs/get_list': 'Просмотр записей логирования отправки',
  'send_logs/get': 'Просмотр лога отправки',
  'send_logs/add': 'Добавление лога отправки',
  'send_logs/edit': 'Редактирование лога отправки',
  'send_logs/get_xlsx': 'Загрука Excel лога отправки',
  'send_logs/get_xlsx_ua': 'Загрука Excel лога действий пользователя',
  'send_logs/get_list_ua': 'Просмотр списка действий пользователя',
  'users/get_list': 'Просмотр списка пользователей',
  'users/get': 'Просмотр пользователя',
  'users/add': 'Добавление пользователя',
  'users/edit': 'Редактирование пользователя',
  'roles/get_list': 'Просмотр списка ролей',
  'roles/add': 'Добавление роли',
  'roles/edit': 'Редактирование роли',
  'users/login': 'Вход в систему'
}

