from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from ..models.SendLogs import SendLogsModel
from ..schemas.SendLogs import SendLogsScheme
import logging


async def add(
        send_logs_request: SendLogsScheme,
        db=Depends(get_session_sync)
):
    if 'id' in send_logs_request:
        del send_logs_request.id
    db_send_logs = SendLogsModel(
        user_id=send_logs_request.user_id,
        created_at=send_logs_request.created_at,
        customer_id=send_logs_request.customer_id,
        date=send_logs_request.date,
        instruction_id=send_logs_request.instruction_id,
        status=send_logs_request.status
    )
    db.add(db_send_logs)
    db.commit()
    db.refresh(db_send_logs)

    return db_send_logs
