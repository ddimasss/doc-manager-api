from pydantic import BaseModel, Field
from typing import List, Optional, Any
from datetime import datetime, date
from app.api.customer.schemas.Customer import CustomerSchemeIn
from typing import Any


# CREATE TABLE public.positions (
# 	id serial4 NOT NULL,
# 	include_date date NOT NULL,
# 	exclude_date date NULL,
# 	description text NULL,
# 	name_i varchar NOT NULL,
# 	name_r varchar NOT NULL,
# 	name_d varchar NOT NULL,
# 	name_t varchar NOT NULL,
# 	name_v varchar NOT NULL,
# 	CONSTRAINT positions_pk PRIMARY KEY (id)
# );


class SendLogsScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    user_id: int
    created_at: datetime
    customer_id: int
    date: date
    instruction_id: int
    status: str
    customer: CustomerSchemeIn
    user: Any
    instruction: Any

