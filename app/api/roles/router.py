from fastapi import APIRouter
from .actions.add import add
from .actions.edit import edit
from .actions.get import get_list
from .schemas.Role import RoleTypeScheme
from typing import List


role_router = APIRouter()


role_router.add_api_route(
    path='/add',
    endpoint=add,
    methods=['POST'],
    tags=['other'],
    response_model=RoleTypeScheme
)
role_router.add_api_route(
    path='/edit',
    endpoint=edit,
    methods=['POST'],
    tags=['other'],
    response_model=RoleTypeScheme
)
role_router.add_api_route(
    path='/get_list',
    endpoint=get_list,
    methods=['GET'],
    tags=['other'],
    response_model=List[RoleTypeScheme]
)
