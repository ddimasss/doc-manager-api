import logging
from typing import List
from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Roles import PermissionType, RoleTypeModel
from ..schemas.Role import RoleTypeScheme
from app.api.users.models.Users import UserModel, PermisssionModel
from sqlalchemy import func
from sqlalchemy import delete, insert, select, update

async def edit(
        role_request: RoleTypeScheme,
        db=Depends(get_session_sync)
):
    role: RoleTypeModel = db.query(RoleTypeModel).get(role_request.id)
    role.title = role_request.title
    role.permissions = role_request.permissions

    db.merge(role)
    db.flush()


    delete_old_permissions = db.query(PermisssionModel). \
        where(UserModel.id == PermisssionModel.user_id). \
        where(UserModel.role_id == role.id). \
        delete()

    db.flush()

    users_to_update: List[UserModel] = db.query(UserModel).where(UserModel.role_id==role.id).all()

    for user in users_to_update:
        for permission in role.permissions:
            permission_model = PermisssionModel(
                name=permission,
                user_id=user.id
            )
            db.add(permission_model)

    db.commit()

    role.permissions = [x.name for x in role.permissions]
    return role
