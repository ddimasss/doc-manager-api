from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from app.api.roles.models.Roles import RoleTypeModel
from ..schemas.Role import RoleTypeScheme, PermissionType
import logging


async def add(
        role_request: RoleTypeScheme,
        db=Depends(get_session_sync)
):
    db_role = RoleTypeModel(
        title=role_request.title,
        permissions=role_request.permissions
    )
    db.add(db_role)

    db.commit()
    db.refresh(db_role)

    db_role.permissions = [x.name for x in db_role.permissions]
    return db_role
