from fastapi import Depends

from app.service.sqlalchemy import get_session_sync
from ..models.Roles import RoleTypeModel
from ..schemas.Role import RoleTypeScheme

async def get_list(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit

    obj = db.query(RoleTypeModel) \
        .group_by(RoleTypeModel.id) \
        .limit(limit) \
        .offset(offset) \
        .all()

    list_ = []
    for value in obj:
        list_.append(
            RoleTypeScheme(
                id=value.id,
                title=value.title,
                permissions=[x.name for x in value.permissions]
                )
        )

    return list_
