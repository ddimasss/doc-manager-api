from pydantic import BaseModel
from typing import List, Optional
import enum

class PermissionType(enum.Enum):
    customers_view = "customers_view"
    customers_edit = "customers_edit"
    customer_send = "customer_send"
    users_view = "users_view"
    users_edit = "users_edit"
    settings_edit = "settings_edit"
    settings_view = "settings_view" 

class RoleTypeScheme(BaseModel):
    id: Optional[int] = None
    title: str
    permissions: List[PermissionType]

    class Config:
        from_attributes = True
        use_enum_values = True
