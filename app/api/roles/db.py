from sqlalchemy.ext.declarative import as_declarative


@as_declarative()
class Base:
    __tablename__ = 'roles'
    __table_args__ = {'schema': 'public'}
