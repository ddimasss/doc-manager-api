from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_USERS_'


class SdlSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


roles_settings = SdlSettings()
