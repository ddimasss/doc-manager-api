from sqlalchemy import String, Column, Integer, ForeignKey, Sequence
from sqlalchemy.dialects.postgresql import ARRAY, ENUM
from app.api.users.models.Users import UserModel 
from app.service.sqlalchemy import BaseAlch
from sqlalchemy_serializer import SerializerMixin
from app.api.users.schemas.User import PermissionType

class RoleTypeModel(BaseAlch, SerializerMixin):
    __tablename__ = 'role_types'
    role_type_id_seq = Sequence('role_type_id_seq')
    id = Column(Integer, role_type_id_seq, server_default=role_type_id_seq.next_value(), primary_key=True)
    title = Column(String, nullable=True)
    permissions = Column(ARRAY(ENUM(PermissionType)))

class RoleModel(BaseAlch, SerializerMixin):
    __tablename__ = 'roles'
    role_id_seq = Sequence('role_id_seq') # TODO delete primary key
    id = Column(Integer, role_id_seq, server_default=role_id_seq.next_value(), primary_key=True)
    user_id = Column(Integer, ForeignKey(UserModel.id), nullable=False)
    role_type_id = Column(Integer, ForeignKey(RoleTypeModel.id), nullable=False)
