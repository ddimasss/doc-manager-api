from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_USERS_'


class SdlSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


employee_settings = SdlSettings()
