from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Users import UserModel, PermisssionModel
from app.api.roles.models.Roles import RoleTypeModel
from ..schemas.User import UserScheme


async def edit(
        user_request: UserScheme,
        db=Depends(get_session_sync)
):

    obj: UserModel = db.query(UserModel).get(user_request.id)
    obj.email = user_request.email
    obj.description = user_request.description
    obj.first_name = user_request.first_name
    obj.last_name = user_request.last_name
    obj.sur_name = user_request.sur_name
    obj.password = user_request.password
    obj.is_deleted = user_request.is_deleted
    obj.role_id = user_request.role_id
    db.merge(obj)
    db.flush()

    old_permissions = db.query(PermisssionModel). \
        filter(PermisssionModel.user_id == user_request.id). \
            delete()
    db.flush()

    role_type = db.query(RoleTypeModel) \
    .where(RoleTypeModel.id == user_request.role_id) \
    .first()

    if role_type:
        for permission in role_type.permissions:
            permission_model = PermisssionModel(
                name=permission,
                user_id=obj.id
            )
            db.add(permission_model)

    db.commit()
    obj.permissions = user_request.permissions
    return obj
