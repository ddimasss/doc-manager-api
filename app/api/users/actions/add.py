from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from ..models.Users import UserModel, PermisssionModel
from app.api.roles.models.Roles import RoleTypeModel
from ..schemas.User import UserScheme
import logging


async def add(
        user_request: UserScheme,
        db=Depends(get_session_sync)
):
    if 'id' in user_request:
        del user_request.id

    db_user = UserModel(
        email=user_request.email.lower(),
        first_name=user_request.first_name,
        last_name=user_request.last_name,
        sur_name=user_request.sur_name,
        password=user_request.password,
        description=user_request.description,
        role_id=user_request.role_id,
    )
    db.add(db_user)
    db.flush()

    if user_request.role_id:
        role_type = db.query(RoleTypeModel) \
            .where(RoleTypeModel.id == user_request.role_id) \
            .first()

        for permission in role_type.permissions:
            permission_model = PermisssionModel(
                name=permission,
                user_id=db_user.id
            )
            db.add(permission_model)

    # Из-за бага в sqlalchemy без роли не работает join в
    # doc-manager-api/app/api/users/actions/get.py", line 40, in get_list
    # по этому нужно давать начальное разрешение
    else:
        permission_model = PermisssionModel(
            name='customers_view',
            user_id=db_user.id
        )
        db.add(permission_model)


    db.commit()
    db.refresh(db_user)

    return db_user
