from sqlalchemy import String, Column, Integer, ForeignKey, Sequence, Boolean
from sqlalchemy.dialects.postgresql import ARRAY, ENUM
from sqlalchemy.orm import relationship

from app.service.sqlalchemy import BaseAlch
from sqlalchemy_serializer import SerializerMixin
from app.api.users.schemas.User import PermissionType

class UserModel(BaseAlch, SerializerMixin):
    __tablename__ = 'users'
    users_id_seq = Sequence('users_id_seq')
    id = Column(Integer, users_id_seq, server_default=users_id_seq.next_value(), primary_key=True)
    email = Column(String, nullable=False)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    sur_name = Column(String, nullable=True)
    password = Column(String, nullable=False)
    role_id = Column(Integer, nullable=True)
    description = Column(String, nullable=True)
    is_deleted = Column(Boolean, nullable=False, default=False)
    # permissions = relationship('PermisssionModel')


class PermisssionModel(BaseAlch, SerializerMixin):
    __tablename__ = 'permissions'
    permissions_id_seq = Sequence('permissions_id_seq')
    id = Column(Integer, permissions_id_seq, server_default=permissions_id_seq.next_value(), primary_key=True)
    name = Column(ENUM(PermissionType), nullable=False)
    type = Column(String, nullable=True)
    user_id = Column(Integer, ForeignKey(UserModel.id), nullable=False)
