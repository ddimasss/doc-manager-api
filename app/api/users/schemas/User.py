from pydantic import BaseModel
from typing import List, Optional, Literal
import enum

class LoginScheme(BaseModel):
    class Config:
        from_attributes = True
    email: str
    password: str

class PermissionType(enum.Enum):
    customers_view = enum.auto()
    customers_edit = enum.auto()
    customer_send = enum.auto()
    users_view = enum.auto()
    users_edit = enum.auto()
    settings_edit = enum.auto()
    settings_view = enum.auto() 

class PermissionsScheme(BaseModel):
    class Config:
        from_attributes = True

    id: Optional[int] = None
    name: Literal[
                'customers_view',
                'customers_edit',
                'customer_send',
                'users_view',
                'users_edit',
                'settings_edit',
                'settings_view'
            ]
    type: Optional[str]
    user_id: int


class UserScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    email: str
    first_name: str
    last_name: str
    sur_name: Optional[str] = None
    password: str
    description: Optional[str] = None
    is_deleted: bool = False
    role_id: Optional[int] = None
    permissions: Optional[
        List[
            Literal[
                'customers_view',
                'customers_edit',
                'customer_send',
                'users_view',
                'users_edit',
                'settings_edit',
                'settings_view'
            ]
        ]
    ] = []
