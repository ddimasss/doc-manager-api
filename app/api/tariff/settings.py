from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_TARIFF_'


class TariffSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


tariff_settings = TariffSettings()