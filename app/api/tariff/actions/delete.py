from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Tariff import TariffModel
from ..schemas.Tariff import TariffScheme


async def edit(
        tariff_request: TariffScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(TariffModel).get(tariff_request.id)
    obj.name = tariff_request.name
    obj.description = tariff_request.description
    obj.is_deleted = tariff_request.is_deleted
    db.commit()

    return obj
