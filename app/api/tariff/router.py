from fastapi import APIRouter
from .actions.add import add
from .actions.delete import edit
from .actions.get import get, get_list
from .schemas.Tariff import TariffScheme
from typing import List


tariff_router = APIRouter()


tariff_router.add_api_route(
    path='/add',
    endpoint=add,
    methods=['POST'],
    tags=['other'],
    response_model=TariffScheme
)
tariff_router.add_api_route(
    path='/edit',
    endpoint=edit,
    methods=['POST'],
    tags=['other'],
    response_model=TariffScheme
)

tariff_router.add_api_route(
    path='/get',
    endpoint=get,
    methods=['GET'],
    tags=['other'],
    response_model=TariffScheme
)

tariff_router.add_api_route(
    path='/get_list',
    endpoint=get_list,
    methods=['GET'],
    tags=['other'],
    response_model=List[TariffScheme]
)
