from app.generator.main import get_filename_from_path


def test_unix_path():
    path = '/var/www/template_files/Приказ_ДИ_и_Утв_ПВК_Приказ_ДИ_и_Утв_ПВК.docx'
    assert get_filename_from_path(path) == 'Приказ_ДИ_и_Утв_ПВК_Приказ_ДИ_и_Утв_ПВК.docx'

# def test_unix_path_with_trail_slash():
#     path = '/var/www/template_files/Приказ_ДИ_и_Утв_ПВК_Приказ_ДИ_и_Утв_ПВК.docx/'
#     assert get_filename_from_path(path) == 'Приказ_ДИ_и_Утв_ПВК_Приказ_ДИ_и_Утв_ПВК.docx'
