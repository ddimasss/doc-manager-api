import orjson
from sqlalchemy import String, Column, Integer, DateTime, Sequence
from sqlalchemy.dialects.postgresql import JSONB
from starlette.datastructures import Headers
from starlette.types import ASGIApp, Scope, Receive, Send

from app.service.sqlalchemy import BaseAlch, get_session
from app.settings import settings
from sqlalchemy_serializer import SerializerMixin
from datetime import datetime
from fastapi import Request
from starlette.middleware.base import BaseHTTPMiddleware


class UserActionsLogsModel(BaseAlch, SerializerMixin):
    __tablename__ = 'actions_logs'
    actions_logs__id_seq = Sequence('actions_logs_id_seq')
    id = Column(Integer, actions_logs__id_seq, server_default=actions_logs__id_seq.next_value(), primary_key=True)
    user_email = Column(String, nullable=False)
    created_at = Column(DateTime, nullable=False)
    endpoint = Column(String, nullable=False)
    old_data = Column(JSONB)
    new_data = Column(JSONB)



async def add_action_log_to_db(user_email: str, path: str = '', old = None, new = None):
    if user_email is not None:
        sendlogs = UserActionsLogsModel(
            user_email=user_email,
            created_at = datetime.now(),
            endpoint = path,
            old_data = old,
            new_data = new
        )
        session = await get_session().__anext__()
        session.add(sendlogs)
        await session.commit()

async def get_old_new():
    pass


class UserActionsMiddleware(BaseHTTPMiddleware):
    async def set_body(self, request: Request):
        receive_ = await request._receive()

        async def receive():
            return receive_

        request._receive = receive
    async def dispatch(self, request, call_next):
        path = request.scope['path']
        if 'templates/edit_upload' not in path:
            old = new = None
            headers = request.headers
            user_email = headers.get("X-User")
        
            method = request.method
            if method == "POST" and 'login' not in path:
                await self.set_body(request)
                body_bytes = await request.body()
                body = orjson.loads(body_bytes)
                if 'copy' in body:
                    old = body['copy']
                    del body['copy']
                new = body
            await add_action_log_to_db(user_email, path, old, new)

        response = await call_next(request)
        return response


class UserActionsMiddleware1:
    def __init__(self, app: ASGIApp) -> None:
        self.app = app

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        old = new = None
        headers = Headers(scope=scope)
        user_email = headers.get("X-User")
        method = scope["method"]
        if method == "POST":
            req = Request(scope=scope, receive=receive)
            body_bytes = await req.body()
            body = orjson.loads(body_bytes)
            print('UserActionsToDB:56', body)

        path = scope['path']
        await add_action_log_to_db(user_email, path)
        print('before next')
        await self.app(req.scope, req.receive, send)

