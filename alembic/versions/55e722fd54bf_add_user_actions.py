"""Add user_actions

Revision ID: 55e722fd54bf
Revises: e5cca3824030
Create Date: 2023-08-04 17:50:54.805424

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '55e722fd54bf'
down_revision = 'e5cca3824030'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('actions_logs',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_email', sa.String(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('endpoint', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    schema='public'
    )

def downgrade():
    op.drop_table('actions_logs', schema='public')
